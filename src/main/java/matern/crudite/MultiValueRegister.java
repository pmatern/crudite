package matern.crudite;

import matern.crudite.clock.VersionClock;
import matern.crudite.clock.VersionVector;
import matern.crudite.clock.VersionVector.State;
import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.Set;

/**
 *
 */
public class MultiValueRegister<V> {
    private final int replicaId;
    private final VersionVector<V> vv;

    public MultiValueRegister(int replicaId, int numReplicas) {
        this.replicaId = replicaId;
        this.vv = new VersionVector<>(numReplicas);
    }
    
    public boolean set(V value, byte[] versionToken) {
        return vv.apply(versionClock(value, versionToken), replicaId);
    }
    
    public Optional<RegisterValue<V>> get() {
        Optional<State<V>> vc = vv.latest();
        
        if (vc.isPresent()) {
            State<V> vcVal = vc.get();
            RegisterValue<V> returnVal = new RegisterValue<>(vcVal.getValues(), token(vcVal.getClock()));
            return Optional.of(returnVal);
        } else {
            return Optional.empty();
        }
    }
    
    private <V> VersionClock<V> versionClock(V value, byte[] versionToken) {
        int[] clock = clock(versionToken);
        VersionClock<V> vc = new VersionClock<>(value, clock);
        
        return vc;
    }
    
    private byte[] token(int[] clock) {
        ByteBuffer buff = ByteBuffer.allocate(clock.length * Integer.SIZE);
        for (int i=0; i<clock.length; i++) {
            buff.putInt(clock[i]);
        }
        
        return buff.array();
    }
    
    private int[] clock(byte[] token) {
        int numReplicas = Math.max(token.length / Integer.SIZE, vv.numReplicas());
        int[] clock = new int[numReplicas];
        
        ByteBuffer buff = ByteBuffer.wrap(token);
        for (int i=0; i<numReplicas && buff.remaining() >= Integer.SIZE; i++) {
            clock[i] = buff.getInt();
        }
        
        return clock;
    }
    
    public static class RegisterValue<V> {
        private final Set<V> values;
        private final byte[] context;

        private RegisterValue(Set<V> values, byte[] context) {
            this.values = values;
            this.context = context;
        }

        public Set<V> getValues() {
            return values;
        }

        public byte[] getContext() {
            return context;
        }
    }
    
}
