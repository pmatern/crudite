package matern.crudite.counter;

/**
 * A positive/negative counter which allows out of order operations to converge.
 */
public class BitSetPNCounter {

    private final MinimalBitSet incs;
    private final MinimalBitSet decs;


    public BitSetPNCounter(BitSetProvider bsp) {
        this.incs = bsp.newBitSet();
        this.decs = bsp.newBitSet();
    }
    
    private void update(MinimalBitSet ops, long opId) {
        if (opId < 0) {
            throw new IllegalArgumentException("operation id must be >= 0");
        }
        
        ops.set(opId);
    }

    public void increment(long incId) {
        update(incs, incId);
    }

    public void decrement(long decId) {
        update(decs, decId);
    }

    public long value() {
        return incs.cardinality() - decs.cardinality();
    }
}
