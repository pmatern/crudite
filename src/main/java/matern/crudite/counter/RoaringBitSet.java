package matern.crudite.counter;

import org.roaringbitmap.RoaringBitmap;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Impl backed by roaring bitmap
 */
public class RoaringBitSet implements MinimalBitSet {

    private final RoaringBitmap lowMap = new RoaringBitmap();
    private final ReadWriteLock lowLock = new ReentrantReadWriteLock();
    private final RoaringBitmap highMap = new RoaringBitmap();
    private final ReadWriteLock highLock = new ReentrantReadWriteLock();

    @Override
    public void set(long ordinal) {
        if (ordinal < 0) {
            throw new IllegalArgumentException("ordinal must be >= 0");
        }

        if (ordinal > Integer.MAX_VALUE) {
            int bit = (int) (ordinal - Integer.MAX_VALUE);
            update(highMap, highLock, bit);
        } else {
            update(lowMap, lowLock, (int) ordinal);
        }
    }

    @Override
    public boolean testAndSet(long ordinal) {
        if (ordinal < 0) {
            throw new IllegalArgumentException("ordinal must be >= 0");
        }

        if (ordinal > Integer.MAX_VALUE) {
            int bit = (int) (ordinal - Integer.MAX_VALUE);
            return conditionalUpdate(highMap, highLock, bit);
        } else {
            return conditionalUpdate(lowMap, lowLock, (int) ordinal);
        }
    }
    
    private void update(RoaringBitmap bitMap, ReadWriteLock rwl, int bit) {
        try {
            rwl.writeLock().lock();
            bitMap.add(bit);
        } finally {
            rwl.writeLock().unlock();
        }
    }

    private boolean conditionalUpdate(RoaringBitmap bitMap, ReadWriteLock rwl, int bit) {
        try {
            rwl.writeLock().lock();
            if (bitMap.contains(bit)) {
                return false;
            } else {
                bitMap.add(bit);
                return true;
            }
        } finally {
            rwl.writeLock().unlock();
        }
    }

    @Override
    public long cardinality() {
        try {
            lowLock.readLock().lock();
            highLock.readLock().lock();
            return lowMap.getCardinality() + highMap.getCardinality();
        } finally {
            highLock.readLock().unlock();
            lowLock.readLock().unlock();
        }
    }
}
