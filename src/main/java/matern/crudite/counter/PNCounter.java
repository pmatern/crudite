package matern.crudite.counter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * counter allowing varying increments and relying on unique operation ids per instance
 */
public final class PNCounter {

    private final MinimalBitSet increments;
    private final AtomicLong incVal;
    private final MinimalBitSet decrements;
    private final AtomicLong decVal;

    public PNCounter(BitSetProvider provider) {
        this.increments = provider.newBitSet();
        this.incVal = new AtomicLong();
        this.decrements = provider.newBitSet();
        this.decVal = new AtomicLong();
    }

    public PNCounter(BitSetProvider provider, long amount, long snowflake) {
        this(provider);
        increment(amount, snowflake);
    }

    public void increment(long amount, long snowflake) {
        if (amount < 0) {
            update(decrements, decVal, -amount, snowflake);
        } else {
            update(increments, incVal, amount, snowflake);
        }
    }

    public void decrement(long amount, long snowflake) {
        if (amount < 0) {
            update(increments, incVal, -amount, snowflake);
        } else {
            update(decrements, decVal, amount, snowflake);
        }
    }

    public long value() {
        return incVal.longValue() - decVal.longValue();
    }

    private void update(MinimalBitSet bitSet, AtomicLong counter, long amount, long snowflake) {
        if (bitSet.testAndSet(snowflake)) {
            counter.addAndGet(amount);
        }
    }

}
