package matern.crudite.counter;

import java.util.concurrent.atomic.AtomicInteger;
import org.cliffc.high_scale_lib.NonBlockingHashMapLong;

/**
 * Bit set which uses a set of integers to represent ranges of ordinal positions
 */
public class LazyCompressedBitSet implements MinimalBitSet {

    private static final int numBuckets = Integer.MAX_VALUE / Integer.SIZE;
    private final NonBlockingHashMapLong<AtomicInteger> buckets;

    public LazyCompressedBitSet() {
        buckets = new NonBlockingHashMapLong<>();
        buckets.put(0, new AtomicInteger());
    }

    @Override
    public void set(long ordinal) {
        long bucketPos = ordinal / Long.SIZE;
        AtomicInteger bucketBits = bucket(bucketPos);

        while (true) {
            int before = bucketBits.get();
            int after = before | mask(ordinal, bucketPos);
            if (bucketBits.compareAndSet(before, after)) {
                break;
            }
        }
    }

    @Override
    public boolean testAndSet(long ordinal) {
        long bucketPos = ordinal / Long.SIZE;
        AtomicInteger bucketBits = bucket(bucketPos);

        while (true) {
            int mask = mask(ordinal, bucketPos);
            int before = bucketBits.get();
            if ((before & mask) > 0) {
                return false;
            }
            int after = before | mask;
            if (bucketBits.compareAndSet(before, after)) {
                return true;
            }
        }
    }

    @Override
    public long cardinality() {
        long cardinality = 0;
        for (AtomicInteger bucketBits : buckets.values()) {
            cardinality += Integer.bitCount(bucketBits.get());
        }

        return cardinality;
    }
    

    private int mask(long bitPos, long bucketPos) {
        bitPos = bitPos - (bucketPos * Integer.SIZE);
        return 1 << bitPos;
    }

    private AtomicInteger bucket(long bucketPos) {
        AtomicInteger b = buckets.get(bucketPos);

        if (b != null) {
            return b;
        } else {
            AtomicInteger maybe = new AtomicInteger();
            b = buckets.putIfAbsent(bucketPos, maybe);
            if (b == null) {
                b = maybe;
            }

            if (buckets.size() > numBuckets) {
                throw new IllegalStateException("Expected no more than " + numBuckets + " internal buckets in bit set");
            }

            return b;
        }
    }

}
