package matern.crudite.counter;

import com.boundary.high_scale_lib.NonBlockingHashSetLong;

public class OrdinalBitSet implements MinimalBitSet {
    private final NonBlockingHashSetLong bitSet = new NonBlockingHashSetLong();

    @Override
    public void set(long ordinal) {
        testAndSet(ordinal);
    }

    @Override
    public boolean testAndSet(long ordinal) {
        return bitSet.add(ordinal);
    }

    @Override
    public long cardinality() {
        return bitSet.size();
    }
}
