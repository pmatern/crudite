package matern.crudite.counter;


public interface BitSetProvider {
    MinimalBitSet newBitSet();
}
