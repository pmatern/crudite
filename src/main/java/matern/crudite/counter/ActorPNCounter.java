package matern.crudite.counter;

import org.cliffc.high_scale_lib.NonBlockingHashMapLong;

/**
 * counter which splits operations by actor id, allowing less unique snowflake values
 */
public final class ActorPNCounter {
    private final NonBlockingHashMapLong<PNCounter> counts = new NonBlockingHashMapLong<>();
    private final BitSetProvider bsp;

    public ActorPNCounter(BitSetProvider bsp) {
        this.bsp = bsp;
    }
    
    public ActorPNCounter(BitSetProvider bsp, long initialAmount, long initialActor, long initialsnowflake) {
        this(bsp);
        increment(initialAmount, initialActor, initialsnowflake);
    }
    
    public void increment(long amount, long actor, long snowflake) {
        if (amount < 0) {
            decrement(-amount, actor, snowflake);
        }
        else {
            getActorCounts(actor).increment(amount, snowflake);
        }
    }
    
    
    public void decrement(long amount, long actor, long snowflake) {
        if (amount < 0) {
            increment(-amount, actor, snowflake);
        }
        else {
            getActorCounts(actor).decrement(amount, snowflake);
        }
    }
    
    private PNCounter getActorCounts(long actor) {
        PNCounter actorCounts = counts.get(actor);
        if (actorCounts == null) {
            actorCounts = new PNCounter(bsp);
            
            PNCounter existing = counts.putIfAbsent(actor, actorCounts);
            if (existing != null) {
                actorCounts = existing;
            }
        }
        
        return actorCounts;
    }
    
    public long value() {
        long accumulate = 0;
        
        for (PNCounter incDec : counts.values()) {
            accumulate += incDec.value();
        }
        
        return accumulate;
    }
    
}
