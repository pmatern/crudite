package matern.crudite.counter;


public interface MinimalBitSet {
    void set(long ordinal);
    
    boolean testAndSet(long ordinal);

    long cardinality();
}
