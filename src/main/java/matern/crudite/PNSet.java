package matern.crudite;

import com.google.common.collect.Iterables;
import matern.crudite.counter.BitSetPNCounter;
import matern.crudite.counter.BitSetProvider;
import java.util.Collection;
import java.util.Map;
import org.cliffc.high_scale_lib.NonBlockingHashMap;

/**
 * PNCounter based set. Value type must support equals and hashcode.
 */
public class PNSet<T> {
    private final BitSetProvider bsp;
    private final NonBlockingHashMap<T, BitSetPNCounter> entries = new NonBlockingHashMap<>();

    public PNSet(BitSetProvider bsp) {
        this.bsp = bsp;
    }
    
    public void add(T element, long snowflake) {
        BitSetPNCounter initial = new BitSetPNCounter(bsp);
        BitSetPNCounter existing = entries.putIfAbsent(element, initial);
        if (existing != null) {
            initial = existing;
        }
        
        initial.increment(snowflake);
        
    }
    
    public void multiAdd(Collection<T> elements, long snowflake) {
        for (T element : elements) {
            add(element, snowflake);
        }
    }
    
    public void remove(T element, long snowflake) {
        BitSetPNCounter existing = entries.get(element);
        if (existing == null) {
            BitSetPNCounter initial = new BitSetPNCounter(bsp);
            existing = entries.putIfAbsent(element, initial);
            if (existing == null) {
                existing = initial;
            }
        }
        
        existing.decrement(snowflake);
    }
    
    public void multiRemove(Collection<T> elements, long snowflake) {
        for (T element : elements) {
            remove(element, snowflake);
        }
    }
    
    public boolean contains(T element) {
        BitSetPNCounter existing = entries.get(element);
        return existing != null && existing.value() > 0;
    }
    
    public Iterable<T> values() {
        return Iterables.transform( Iterables.filter(entries.entrySet(), (Map.Entry<T, BitSetPNCounter> t) -> {
            return t.getValue().value() > 0;
        }), (Map.Entry<T, BitSetPNCounter> f) -> {
            return f.getKey();
        });
    }
    
}
