package matern.crudite.util;

public interface SnowflakeProvider {
    long EPOCH = 1349734204785L; // Mon Oct 8, 1012 EOA epoch
    
    long nextSnowflake();
    
}
