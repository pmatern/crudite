package matern.crudite.util;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Package private timestamper which guarantees no dupes or regressions.
 */
public final class Timestamper {

    private final AtomicLong counter = new AtomicLong();

    public long timestamp() {
        while (true) {
            long then = counter.get();
            long now = System.currentTimeMillis();
            long later = now > then ? now : then + 1;
            if (counter.compareAndSet(then, later)) {
                return later;
            }
        }
    }
}
