package matern.crudite.util;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Generates snowflakes using a combination of system time, a logical writer id, and an incrementing sequence number.
 */
public final class SnowflakeProviderImpl implements SnowflakeProvider {

    private final int maxOrderId;
    private final SnowflakeIdPacker idPacker;
    private final int writerId;
    private final AtomicReference<TimeAndOrder> state;
    private final AtomicLong counter;

    public SnowflakeProviderImpl(int writerId, SnowflakeIdPacker idPacker) {
        this.counter = new AtomicLong();
        
        int maxWritterId = (int) Math.pow(2, idPacker.bitsPrecisionOfWriterId()) - 1;
        if (writerId < 0 || writerId > maxWritterId) {
            throw new IllegalArgumentException("writerId is out of range must be 0.." + maxWritterId);
        }
        
        this.writerId = writerId;
        this.idPacker = idPacker;
        this.maxOrderId = (int) Math.pow(2, idPacker.bitsPrecisionOfOrderId()) - 1;
        this.state = new AtomicReference<>(new TimeAndOrder(timestamp(), 0));
    }
    
    private long timestamp() {
        return System.currentTimeMillis() - EPOCH;
    }
//    private long timestamp() {
//        while (true) {
//            long then = counter.get();
//            long now = System.currentTimeMillis();
//            long later = now > then ? now : then + 1;
//            if (counter.compareAndSet(then, later)) {
//                return later - EPOCH;
//            }
//        }
//    }

    @Override
    public long nextSnowflake() {
        while (true) { // exits on successful compare-and-set
            long timestamp = timestamp();
            TimeAndOrder current = state.get();

            if (current.time > timestamp) {
                long retryWaitHint = current.time - timestamp;

                try {
                    Thread.sleep(retryWaitHint);
                } catch (InterruptedException ie) {
                    Thread.interrupted();
                }

            } else {
                TimeAndOrder next;

                if (current.time == timestamp) {
                    int nextOrder = current.order + 1;

                    if (nextOrder > maxOrderId) {
                        continue;
                    }

                    next = new TimeAndOrder(timestamp, nextOrder);
                } else {
                    next = new TimeAndOrder(timestamp, 0);
                }

                if (state.compareAndSet(current, next)) {
                    return idPacker.pack(next.time, writerId, next.order);
                }
            }
        }
    }

    //if you want to be annoying, you could avoid this object by packing the state in a long and test/set an AtomicLong
    private static final class TimeAndOrder {

        TimeAndOrder(long time, int order) {
            this.time = time;
            this.order = order;
        }
        final long time;
        final int order;
    }
}
