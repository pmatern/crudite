package matern.crudite.util;

public class SnowflakeIdPacker  {

    public int bitsPrecisionOfOrderId() {
        return 12;
    }

    public int bitsPrecisionOfTimestamp() {
        return 42;
    }

    public int bitsPrecisionOfWriterId() {
        return 10;
    }

    public long pack(long timestamp, int writerId, int orderId) {
        long id = (timestamp & 0x3FFFFFFFFFFL) << 10 + 12;
        id |= ((writerId & 0x3FF) << 12);
        id |= (orderId & 0xFFF);
        return id;
    }   

    public long[] unpack(long packedId) {
        long packed = packedId;
        long time = (packed & (0x3FFFFFFFFFFL << 10 + 12)) >>> 10 + 12;
        int writer = (int) ((packed & (0x3FF << 12)) >>> 12);
        int order = (int) (packed & 0xFFF);
        return new long[]{time, writer, order};
    }

}

