package matern.crudite.clock;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 * @param <V>
 */
public final class VersionVector<V> {
    private final int numReplicas;
    private final Set<VersionClock<V>> siblings = new CopyOnWriteArraySet<>();

    public VersionVector(int numReplicas) {
        this.numReplicas = numReplicas;
    }
    
    public boolean apply(VersionClock<V> vc, int replicaId) {
        if (siblings.isEmpty()) {
            vc = vc.rollVersion(replicaId, 1);
            siblings.add(vc);
            return true;
        } else {
            
            boolean addVC = false;
            for (VersionClock<V> sibling : siblings) {
                Optional<VersionClock<V>> winner = VersionClock.determineWinner(vc, sibling);

                if (winner.isPresent()) {
                    VersionClock<V> winnerValue = winner.get();
                    if (winnerValue.equals(vc)) {
                        siblings.remove(sibling);
                        addVC = true;
                    }
                } else if (vc.equals(sibling)) {
                    siblings.remove(sibling);
                    addVC = true;
                } else {
                    addVC = true;
                }
            }
            
            if (addVC) {
                vc = vc.rollVersion(replicaId, 1);
                siblings.add(vc);
            }
            
            return addVC;
        }
    }
    
    public int numReplicas() {
        return numReplicas;
    }
    
    public Set<V> values() {
        Set<V> result = new HashSet<>();
        siblings.stream().map(vc -> vc.getValue()).forEach(v -> result.add(v));
        
        return result;
    }
    
    public Optional<State<V>> latest() {
        State<V> result = new State<>(numReplicas);
        
        for (VersionClock<V> clock : siblings) {
            result.addValue(clock.getValue());
            
            for (int i=0; i<clock.getNumReplicas(); i++) {
                result.addVersion(i, clock.getVersion(i));
            }
        }
        
        return (result.values.isEmpty()) ? Optional.empty() : Optional.of(result);
    }
    
    public static class State<V> {
        private final Set<V> values;
        private final int[] clock;
        
        private State(int numReplicas) {
            this.values = new HashSet<>();
            this.clock = new int[numReplicas];
        }
        
        private void addValue(V value) {
            values.add(value);
        }
        
        private void addVersion(int replicaId, int version) {
            if (replicaId > -1 && replicaId < clock.length) {
                clock[replicaId] = Math.max(clock[replicaId], version);
            } else {
                throw new IllegalArgumentException("Invalid replicaId " + replicaId);
            }
        }
        
        public Set<V> getValues() {
            return values;
        }
        
        public int[] getClock() {
            return clock;
        }
        
    }
    
}
