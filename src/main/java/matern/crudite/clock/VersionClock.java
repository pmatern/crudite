package matern.crudite.clock;

import java.util.Arrays;
import java.util.Optional;


/**
 *
 * @param <V>
 */
public final class VersionClock<V> {
    private final V value;
    private final int[] versions;
    
    public VersionClock(V value, int[] clock) {
        this.value = value;
        this.versions = new int[clock.length];
        System.arraycopy(clock, 0, this.versions, 0, clock.length);
    }

    public VersionClock(V value, int numReplicas) {
        this.value = value;
        this.versions = new int[numReplicas];
    }
    
    public VersionClock(VersionClock<V> toCopy, V newValue) {
        this.value = newValue != null ? newValue : toCopy.value; //not really copying...
        this.versions = new int[toCopy.versions.length];
        System.arraycopy(toCopy.versions, 0, this.versions, 0, toCopy.versions.length);
    }
    
    public V getValue() {
        return value;
    }
    
    public int getNumReplicas() {
        return versions.length;
    }
    
    public int getVersion(int replicaId) {
        if (replicaId > -1 && replicaId < versions.length) {
            return versions[replicaId];
        } else {
            throw new IllegalArgumentException("Invalid replicaId " + replicaId);
        }
    }
    
    public VersionClock rollVersion(int replicaId, int increment) {
        if (replicaId > -1 && replicaId < versions.length) {
            VersionClock vc = new VersionClock(this.value, this.versions);
            vc.versions[replicaId] = vc.versions[replicaId] + increment;
            
            return vc;
        } else {
            throw new IllegalArgumentException("Invalid replicaId " + replicaId);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Arrays.hashCode(this.versions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VersionClock<?> other = (VersionClock<?>) obj;
        if (!Arrays.equals(this.versions, other.versions)) {
            return false;
        }
        return true;
    }

    
    public static <V> VersionClock<V> merge(VersionClock<V> a, VersionClock<V> b, V value) {
        int[] vector = new int[(a.versions.length > b.versions.length) ? a.versions.length : b.versions.length];
        
        for (int i=0; i<vector.length; i++) {
            int aVersion = i < a.versions.length ? a.versions[i] : -1;
            int bVersion = i < b.versions.length ? b.versions[i] : -1;
            
            vector[i] = Math.max(aVersion, bVersion);
        }
        
        return new VersionClock<>(value, vector);
    }
    
   
    public static <V> Optional<VersionClock<V>> determineWinner(
            VersionClock<V> a, VersionClock<V> b) {
        
        boolean aTrumps = false;
        boolean bTrumps = false;
        int i = 0;
        
        for (; i<a.versions.length && i<b.versions.length; i++) {
            int aVersion = a.versions[i];
            int bVersion = b.versions[i];
            if (aVersion > bVersion) {
                aTrumps = true;
            } else if (bVersion > aVersion) {
                bTrumps = true;
            }
        }
        
        if (a.versions.length > b.versions.length) {
            aTrumps = true;
        } else if (b.versions.length > a.versions.length) {
            bTrumps = true;
        }
        
        if (aTrumps && bTrumps) {
            return Optional.empty();
        } else if (!aTrumps && !bTrumps) {
            return Optional.empty();
        } else if (aTrumps) {
            return Optional.of(a);
        } else {
            return Optional.of(b);
        }
    }
    
}
