package matern.crudite.clock;

import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 */
public class VersionVectorTest {
    
    @Test
    public void testApplyAndValues() {
        VersionVector<Integer> vv = new VersionVector(3);
        VersionClock<Integer> a = new VersionClock<>(43, 3);
        VersionClock<Integer> b = new VersionClock<>(98, 3);
        VersionClock<Integer> c = new VersionClock<>(36, 3);
        
        a = a.rollVersion(0, 1); 
        b = b.rollVersion(1, 1); 
        c = c.rollVersion(2, 1); 
        
        assertTrue(vv.apply(a, 0));
        assertTrue(vv.apply(b, 1));
        assertTrue(vv.apply(c, 2));
        
        //this mimics what happened internally in vv - pretty hacky
        a = a.rollVersion(0, 1); 
        b = b.rollVersion(1, 1); 
        c = c.rollVersion(2, 1); 
        
        Set<Integer> allThree = vv.values();
        assertNotNull(allThree);
        assertEquals(3, allThree.size());
        assertTrue(allThree.contains(a.getValue()));
        assertTrue(allThree.contains(b.getValue()));
        assertTrue(allThree.contains(c.getValue()));

        VersionClock<Integer> aAndB = VersionClock.merge(a, b, 803); 
        
        assertTrue(vv.apply(aAndB, 0));
        Set<Integer> twoLeft = vv.values();
        assertNotNull(twoLeft);
        assertEquals(2, twoLeft.size());
        assertFalse(twoLeft.contains(a.getValue()));
        assertFalse(twoLeft.contains(b.getValue()));
        assertTrue(twoLeft.contains(c.getValue()));
        assertTrue(twoLeft.contains(aAndB.getValue()));
        
        //this mimics what happened internally in vv - pretty hacky
        aAndB = aAndB.rollVersion(0, 1);
        
        VersionClock<Integer> cPrime = new VersionClock<Integer>(c, 276);
        
        assertTrue(vv.apply(cPrime, 2));
        Set<Integer> stillTwo = vv.values();
        assertNotNull(stillTwo);
        assertEquals(2, stillTwo.size());
        assertFalse(stillTwo.contains(a.getValue()));
        assertFalse(stillTwo.contains(b.getValue()));
        assertFalse(stillTwo.contains(c.getValue()));
        assertTrue(stillTwo.contains(aAndB.getValue()));
        assertTrue(stillTwo.contains(cPrime.getValue()));
        
         //this mimics what happened internally in vv - pretty hacky
        cPrime = cPrime.rollVersion(2, 1);
        
        VersionClock<Integer> aAndBAndC = VersionClock.merge(aAndB, c, 87595);
        
        assertTrue(vv.apply(aAndBAndC, 0));
        stillTwo = vv.values();
        assertNotNull(stillTwo);
        assertEquals(2, stillTwo.size());
        assertFalse(stillTwo.contains(a.getValue()));
        assertFalse(stillTwo.contains(b.getValue()));
        assertFalse(stillTwo.contains(c.getValue()));
        assertFalse(stillTwo.contains(aAndB.getValue()));
        assertTrue(stillTwo.contains(cPrime.getValue()));
        assertTrue(stillTwo.contains(aAndBAndC.getValue()));
        
        //this mimics what happened internally in vv - pretty hacky
        aAndBAndC = aAndBAndC.rollVersion(0, 1);
        
        VersionClock<Integer> finalWinner = VersionClock.merge(aAndBAndC, cPrime, 324);
        
        assertTrue(vv.apply(finalWinner, 0));
        Set<Integer> oneLeft = vv.values();
        assertNotNull(oneLeft);
        assertEquals(1, oneLeft.size());
        assertFalse(oneLeft.contains(a.getValue()));
        assertFalse(oneLeft.contains(b.getValue()));
        assertFalse(oneLeft.contains(c.getValue()));
        assertFalse(oneLeft.contains(aAndB.getValue()));
        assertFalse(oneLeft.contains(cPrime.getValue()));
        assertFalse(oneLeft.contains(aAndBAndC.getValue()));
        assertTrue(oneLeft.contains(finalWinner.getValue()));

    }
}
