package matern.crudite.clock;

import java.util.Optional;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 */
public class VersionClockTest {

    @Test
    public void testBasicComparison() {
        VersionClock<String> a = new VersionClock<>("trees", 2);
        VersionClock<String> b = new VersionClock<>("goats", 2);

        a = a.rollVersion(0, 1);
        b = b.rollVersion(1, 1);

        Optional<VersionClock<String>> winner = VersionClock.determineWinner(a, b);
        assertNotNull(winner);
        assertFalse(winner.isPresent());

        b = b.rollVersion(0, 1);

        winner = VersionClock.determineWinner(a, b);
        assertNotNull(winner);
        assertTrue(winner.isPresent());
        assertEquals(b, winner.get());
        assertEquals("goats", b.getValue());

        a = a.rollVersion(0, 3);
        a = a.rollVersion(1, 1);

        winner = VersionClock.determineWinner(a, b);
        assertNotNull(winner);
        assertTrue(winner.isPresent());
        assertEquals(a, winner.get());
        assertEquals("trees", a.getValue());

        b = b.rollVersion(1, 4);

        winner = VersionClock.determineWinner(a, b);
        assertNotNull(winner);
        assertFalse(winner.isPresent());

        VersionClock<String> merged = VersionClock.merge(a, b, "ladybugs");

        winner = VersionClock.determineWinner(merged, b);
        assertNotNull(winner);
        assertTrue(winner.isPresent());
        assertEquals(merged, winner.get());
        assertEquals("ladybugs", merged.getValue());

        winner = VersionClock.determineWinner(merged, a);
        assertNotNull(winner);
        assertTrue(winner.isPresent());
        assertEquals(merged, winner.get());
        assertEquals("ladybugs", merged.getValue());

    }

}
