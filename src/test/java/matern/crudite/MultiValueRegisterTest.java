package matern.crudite;

import matern.crudite.MultiValueRegister.RegisterValue;
import java.util.Optional;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 */
public class MultiValueRegisterTest {
    
    @Test
    public void testSetAndGet() {
        MultiValueRegister<Float> mvr = new MultiValueRegister<>(1, 3);
        
        Optional<RegisterValue<Float>> empty = mvr.get();
        assertNotNull(empty);
        assertFalse(empty.isPresent());
        
        float firstVal = 45.6f;
        assertTrue(mvr.set(45.6f, new byte[0]));
        assertFalse(mvr.set(76.002f, new byte[0]));
        
        Optional<RegisterValue<Float>> nonEmpty = mvr.get();
        assertNotNull(nonEmpty);
        assertTrue(nonEmpty.isPresent());
        RegisterValue<Float> val = nonEmpty.get();
        
        assertEquals(1, val.getValues().size());
        Float floatVal = val.getValues().iterator().next();
        
        assertEquals(firstVal, floatVal, 0.0f);
        
        byte[] token = val.getContext();
        
        float secondVal = 509.32f;
        assertTrue(mvr.set(secondVal, token));
        assertFalse(mvr.set(123.5f, token));
        
        nonEmpty = mvr.get();
        assertNotNull(nonEmpty);
        assertTrue(nonEmpty.isPresent());
        val = nonEmpty.get();
        
        assertEquals(1, val.getValues().size());
        floatVal = val.getValues().iterator().next();
        
        assertEquals(secondVal, floatVal, 0.0f);
    }
    
    //TODO test with siblings - expose package scope static methods to create tokens that will be concurrent
}
