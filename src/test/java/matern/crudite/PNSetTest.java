package matern.crudite;

import matern.crudite.PNSet;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import matern.crudite.counter.BitSetProvider;
import matern.crudite.counter.RoaringBitSet;
import matern.crudite.util.SnowflakeIdPacker;
import matern.crudite.util.SnowflakeProvider;
import matern.crudite.util.SnowflakeProviderImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 */
public class PNSetTest {

    private final BitSetProvider bsp = () -> new RoaringBitSet();
    private final SnowflakeProvider snp = new SnowflakeProviderImpl(567, new SnowflakeIdPacker());

    @Test
    public void testBasicOps() {
        PNSet<String> set = new PNSet<>(bsp);

        String test = "KANGAROO";
        set.add(test, snp.nextSnowflake());

        assertTrue(set.contains(test));

        set.remove(test, snp.nextSnowflake());

        assertFalse(set.contains(test));

        for (int i = 0; i < 300; i++) {
            set.add(String.valueOf(i), snp.nextSnowflake());
        }

        List<Integer> values = Lists.newArrayList(Iterables.transform(set.values(), (String f) -> Integer.parseInt(f)));

        Collections.sort(values);
        assertEquals(300, values.size());

        for (int i = 0; i < values.size(); i++) {
            assertEquals(i, (int)values.get(i));
        }

        for (int i = 0; i < 300; i++) {
            set.remove(String.valueOf(i), snp.nextSnowflake());
        }

        assertFalse(set.values().iterator().hasNext());
    }

    @Test
    public void testMixedAndConcurrent() throws Exception {
        int numThreads = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        try {
            int numOps = 1024 * 100;

            long[] values = values(numOps);

            int opsPerThread = numOps / numThreads;

            PNSet<Long> set = new PNSet<>(bsp);

            List<Future<SetChecker>> tasks = new ArrayList<>();

            for (int i = 0; i < values.length; i += opsPerThread) {
                tasks.add(executorService.submit(new SetModifier(values, i, opsPerThread, set, true, snp)));
            }

            List<Future<Boolean>> results = new ArrayList<>();
            for (Future<SetChecker> task : tasks) {
                results.add(executorService.submit(task.get()));
            }

            for (Future<Boolean> result : results) {
                assertTrue(result.get());
            }
            results.clear();
            tasks.clear();

            for (int i = 0; i < values.length; i += opsPerThread) {
                tasks.add(executorService.submit(new SetModifier(values, i, opsPerThread, set, false, snp)));
            }
            
            results = new ArrayList<>();
            for (Future<SetChecker> task : tasks) {
                results.add(executorService.submit(task.get()));
            }

            for (Future<Boolean> result : results) {
                assertTrue(result.get());
            }
            results.clear();
            tasks.clear();
            
            
            for (int i = 0; i < values.length; i += opsPerThread) {
                tasks.add(executorService.submit(new SetModifier(values, i, opsPerThread, set, true, snp)));
                tasks.add(executorService.submit(new SetModifier(values, i, opsPerThread, set, false, snp)));
            }

            for (Future task : tasks) {
                task.get();
            }

            assertFalse(set.values().iterator().hasNext());
            tasks.clear();
            
        } finally {
            executorService.shutdownNow();
        }
    }

    private long[] values(int numOps) {
        long[] amounts = new long[numOps];

        for (int i = 0; i < amounts.length; i++) {
            if (i % 2 == 0) {
                amounts[i] = i + Integer.MAX_VALUE;
            } else {
                amounts[i] = i;
            }
        }

        return amounts;
    }

    private static class SetModifier implements Callable<SetChecker> {

        private final long[] values;
        private final int startIdx;
        private final int numOps;
        private final PNSet<Long> set;
        private final boolean add;
        private final SnowflakeProvider timesamper;

        private SetModifier(long[] values, int startIdx, int numOps, PNSet<Long> set, boolean add, SnowflakeProvider timestamper) {
            this.values = values;
            this.startIdx = startIdx;
            this.numOps = numOps;
            this.set = set;
            this.add = add;
            this.timesamper = timestamper;
        }

        @Override
        public SetChecker call() {
            for (int i = 0; i < numOps; i++) {
                long value = values[i + startIdx];
                if (add) {
                    set.add(value, timesamper.nextSnowflake());
                } else {
                    set.remove(value, timesamper.nextSnowflake());
                }
            }

            return new SetChecker(values, startIdx, numOps, set, add);

        }
    }

    private static class SetChecker implements Callable<Boolean> {

        private final long[] values;
        private final int startIdx;
        private final int numOps;
        private final PNSet<Long> set;
        private final boolean expectPresent;

        private SetChecker(long[] values, int startIdx, int numOps, PNSet<Long> set, boolean expectPresent) {
            this.values = values;
            this.startIdx = startIdx;
            this.numOps = numOps;
            this.expectPresent = expectPresent;
            this.set = set;
        }

        @Override
        public Boolean call() throws Exception {
            for (int i = 0; i < numOps; i++) {
                long amount = values[i + startIdx];
                
                if (expectPresent && !set.contains(amount)) {
                    return false;
                } else if (!expectPresent && set.contains(amount)) {
                    return false;
                }
            }

            return true;
        }
    }

}
