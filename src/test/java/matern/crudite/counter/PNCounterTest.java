package matern.crudite.counter;

import matern.crudite.util.Timestamper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 */
public class PNCounterTest {

    private final BitSetProvider bsp = () -> new RoaringBitSet();
    private final Timestamper ts = new Timestamper();

    public void testIncrement() {
        long amount = 789;
        long amount2 = 636;
        long amount3 = 75;
        long amount4 = 9045;

        PNCounter instance = new PNCounter(bsp);
        instance.increment(amount, ts.timestamp());
        instance.increment(amount2, ts.timestamp());

        instance.increment(amount3, ts.timestamp());
        instance.increment(amount4, ts.timestamp());

        long total = amount + amount2 + amount3 + amount4;
        assertEquals(total, instance.value());
    }

    public void testDecrement() {
        long actor = 234;
        long actor2 = 3324;
        long actor3 = 673457;
        long amount = 789;
        long amount2 = 636;
        long amount3 = 75;
        long amount4 = 9045;

        PNCounter instance = new PNCounter(bsp, amount, ts.timestamp());
        instance.decrement(amount2, ts.timestamp());

        instance.decrement(amount3, ts.timestamp());
        instance.decrement(amount4, ts.timestamp());

        long total = amount - (amount2 + amount3 + amount4);
        assertEquals(total, instance.value());

    }

    @Test
    public void testMixedAndConcurrent() throws Exception {
        int numThreads = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        try {
            int numOps = 1024 * 10000;

            long[] amounts = amounts(numOps);

            int opsPerThread = numOps / numThreads;

            PNCounter counter = new PNCounter(bsp);

            List<Future<Long>> tasks = new ArrayList<>();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(amounts, i, opsPerThread, counter, true, ts)));
            }

            long total = 0;
            for (Future<Long> task : tasks) {
                total += task.get();
            }

            Assert.assertEquals(total, counter.value());
            tasks.clear();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(amounts, i, opsPerThread, counter, false, ts)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(amounts, i, opsPerThread, counter, true, ts)));
                tasks.add(executorService.submit(new CounterCaller(amounts, i, opsPerThread, counter, false, ts)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();
        } finally {
            executorService.shutdownNow();
        }
    }

    private long[] amounts(int numOps) {
        long[] amounts = new long[numOps];

        for (int i = 0; i < amounts.length; i++) {
            if (i % 2 == 0) {
                amounts[i] = i + Integer.MAX_VALUE;
            } else {
                amounts[i] = i;
            }
        }

        return amounts;
    }

    private static class CounterCaller implements Callable<Long> {

        private final long[] amounts;
        private final int startIdx;
        private final int numOps;
        private final PNCounter counter;
        private final boolean inc;
        private final Timestamper timesamper;

        private CounterCaller(long[] amounts, int startIdx, int numOps, PNCounter counter, boolean inc, Timestamper timestamper) {
            this.amounts = amounts;
            this.startIdx = startIdx;
            this.numOps = numOps;
            this.counter = counter;
            this.inc = inc;
            this.timesamper = timestamper;
        }

        @Override
        public Long call() {
            long total = 0;

            for (int i = 0; i < numOps; i++) {
                long amount = amounts[i + startIdx];
                total += amount;
                if (inc) {
                    counter.increment(amount, timesamper.timestamp());
                } else {
                    counter.decrement(amount, timesamper.timestamp());
                }
            }
            return total;
        }
    }

}
