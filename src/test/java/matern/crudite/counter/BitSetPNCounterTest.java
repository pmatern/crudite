package matern.crudite.counter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.junit.Assert;
import org.junit.Test;

public final class BitSetPNCounterTest {

    @Test
    public void testBasicIncDec() {
        BitSetPNCounter counter = new BitSetPNCounter(bsp());

        int numOps = 1024 * 10000;
        long[] opIds = opIds(numOps);


        for (long opId : opIds) {
            counter.increment(opId);
        }

        Assert.assertEquals(numOps, counter.value());


        for (long opId : opIds) {
            counter.decrement(opId);
        }

        Assert.assertEquals(0, counter.value());
    }

    @Test
    public void testConcurrentIncAndDec() throws ExecutionException, InterruptedException {
        int numThreads = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        try {
            int numOps = 1024 * 10000;
            final long[] opIds = opIds(numOps);

            int opsPerThread = numOps / numThreads;

            BitSetPNCounter counter = new BitSetPNCounter(bsp());


            List<Future> tasks = new ArrayList<>();

            for (int i = 0; i < opIds.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(opIds, i, opsPerThread, counter, true)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(numOps, counter.value());
            tasks.clear();

            for (int i = 0; i < opIds.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(opIds, i, opsPerThread, counter, false)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();

            for (int i = 0; i < opIds.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(opIds, i, opsPerThread, counter, true)));
                tasks.add(executorService.submit(new CounterCaller(opIds, i, opsPerThread, counter, false)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();
        } finally {
            executorService.shutdownNow();
        }
    }

    private static class CounterCaller implements Runnable {
        private final long[] opIds;
        private final int startIdx;
        private final int numOps;
        private final BitSetPNCounter counter;
        private final boolean inc;

        private CounterCaller(long[] opIds, int startIdx, int numOps, BitSetPNCounter counter, boolean inc) {
            this.opIds = opIds;
            this.startIdx = startIdx;
            this.numOps = numOps;
            this.counter = counter;
            this.inc = inc;
        }

        @Override
        public void run() {
            for (int i = 0; i < numOps; i++) {
                if (inc) {
                    counter.increment(opIds[i+startIdx]);
                } else {
                    counter.decrement(opIds[i+startIdx]);
                }
            }
        }
        
    }


    private BitSetProvider bsp() {
        /*
        return () -> new LazyCompressedBitSet() 
        did 20480000 ops in 1200 millis
        so, 17066 ops per milli
         */ /*
        did 20480000 ops in 743 millis
        so, 27563 ops per milli
        */
        return () -> {
            return new RoaringBitSet();
        };
        //return new OrdinalBitSet();
        
    }


    private long[] opIds(int numOps) {
        long[] opIds = new long[numOps];

        for (int i = 0; i < opIds.length; i++) {
            if (i % 2 == 0) {
                opIds[i] = (long) i + Integer.MAX_VALUE;
            } else {
                opIds[i] = i;
            }
        }

        return opIds;
    }


}
