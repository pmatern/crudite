package matern.crudite.counter;


import matern.crudite.util.Timestamper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class ActorPNCounterTest {
    private final BitSetProvider bsp = ()->new RoaringBitSet();
    private final Timestamper ts = new Timestamper();

    public void testIncrement() {
        long actor = 234;
        long actor2 = 3324;
        long actor3 = 673457;
        long amount = 789;
        long amount2 = 636;
        long amount3 = 75;
        long amount4 = 9045;
        
        ActorPNCounter instance = new ActorPNCounter(bsp);
        instance.increment(amount, actor, ts.timestamp());
        instance.increment(amount2, actor, ts.timestamp());
        
        instance.increment(amount3, actor2, ts.timestamp());
        instance.increment(amount4, actor3, ts.timestamp());
        
        long total = amount + amount2 + amount3 + amount4;
        assertEquals(total, instance.value());
    }
    
    
    public void testDecrement() {
        long actor = 234;
        long actor2 = 3324;
        long actor3 = 673457;
        long amount = 789;
        long amount2 = 636;
        long amount3 = 75;
        long amount4 = 9045;
        
        ActorPNCounter instance = new ActorPNCounter(bsp, amount, actor, ts.timestamp());
        instance.decrement(amount2, actor, ts.timestamp());
        
        instance.decrement(amount3, actor2, ts.timestamp());
        instance.decrement(amount4, actor3, ts.timestamp());
        
        long total = amount - (amount2 + amount3 + amount4);
        assertEquals(total, instance.value());
        
    }
    
    @Test
    public void testMixedAndConcurrent() throws Exception {
        int numThreads = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        try {
            int numOps = 1024 * 10000;
            
            long[] amounts = amounts(numOps);
            
            int opsPerThread = numOps / numThreads;
            

            ActorPNCounter counter = new ActorPNCounter(bsp);

            List<Future<Long>> tasks = new ArrayList<>();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(i, amounts, i, opsPerThread, counter, true, ts)));
            }

            long total = 0;
            for (Future<Long> task : tasks) {
                total += task.get();
            }

            Assert.assertEquals(total, counter.value());
            tasks.clear();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(i, amounts, i, opsPerThread, counter, false, ts)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();

            for (int i = 0; i < amounts.length; i += opsPerThread) {
                tasks.add(executorService.submit(new CounterCaller(i, amounts, i, opsPerThread, counter, true, ts)));
                tasks.add(executorService.submit(new CounterCaller(numThreads+i, amounts, i, opsPerThread, counter, false, ts)));
            }

            for (Future task : tasks) {
                task.get();
            }

            Assert.assertEquals(0, counter.value());
            tasks.clear();
        } finally {
            executorService.shutdownNow();
        }
    }
    
    private long[] amounts(int numOps) {
        long[] amounts = new long[numOps];

        for (int i = 0; i < amounts.length; i++) {
            if (i % 2 == 0) {
                amounts[i] =  i + Integer.MAX_VALUE;
            } else {
                amounts[i] = i;
            }
        }

        return amounts;
    }


    private static class CounterCaller implements Callable<Long> {
        private final long actor;
        private final long[] amounts;
        private final int startIdx;
        private final int numOps;
        private final ActorPNCounter counter;
        private final boolean inc;
        private final Timestamper timesamper;

        private CounterCaller(long actor, long[] amounts, int startIdx, int numOps, ActorPNCounter counter, boolean inc, Timestamper timestamper) {
            this.actor = actor;
            this.amounts = amounts;
            this.startIdx = startIdx;
            this.numOps = numOps;
            this.counter = counter;
            this.inc = inc;
            this.timesamper = timestamper;
        }
        
        @Override
        public Long call() {
            long total = 0;
            
            for (int i = 0; i < numOps; i++) {
                long amount = amounts[i+startIdx];
                total += amount;
                if (inc) {
                    counter.increment(amount, actor, timesamper.timestamp());
                } else {
                    counter.decrement(amount, actor, timesamper.timestamp());
                }
            }
            return total;
        }
    }
    
}
